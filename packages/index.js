import Button from './button';
import Icon from './icon';

const version = '0.0.1';
const components = [
  Button,
  Icon
];

const install = function(Vue) {
  if (install.installed) return;

  components.forEach(component => {
    Vue.component(component.name, component);
  });
};

/* istanbul ignore if */
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

export {
  install,
  version,
  Button,
  Icon
};
export default {
  install,
  version
};
