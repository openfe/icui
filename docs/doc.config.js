/* eslint-disable */
module.exports = {
  "zh-CN": {
    header: {
    },
    footer: {
      github:'https://github.com/icloudcity/iCUi',
      nav: {
       
      }
    },
    nav: [
      {
        "name": "开发指南",
        "groups": [
          {
            "list": [
              {
                "path": "/quickstart",
                "title": "快速上手",
                noExample: true
              }
            ]
          }
        ]
      },
      {
        "name": "组件",
        "showInMobile": true,
        "groups": [
          {
            "groupName": "基础组件",
            "list": [
              {
                "path": "/button",
                "title": "button按钮"
              }
            ]
          },
        ]
      }
    ]
  }
}
