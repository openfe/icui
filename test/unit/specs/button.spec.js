import Button from 'packages/button';
import { mount } from 'avoriaz';

describe('Button', () => {
  let wrapper;

  afterEach(() => {
    wrapper && wrapper.destroy();
  });

  it('创建简单button', () => {
    wrapper = mount(Button);

    expect(wrapper.hasClass('icui-button')).to.be.true;
    expect(wrapper.hasClass('icui-button--default')).to.be.true;
    expect(wrapper.hasClass('icui-button--normal')).to.be.true;

    const eventStub = sinon.stub(wrapper.vm, '$emit');
    wrapper.trigger('click');

    expect(eventStub.calledOnce).to.be.true;
    expect(eventStub.calledWith('click')).to.be.true;
  });


  it('测试创建大按钮', () => {
    wrapper = mount(Button, {
      propsData: {
        size: 'large'
      }
    });

    expect(wrapper.hasClass('icui-button')).to.be.true;
    expect(wrapper.hasClass('icui-button--large')).to.be.true;
  });
  it('创建一个禁止点击的button', () => {
    wrapper = mount(Button, {
      propsData: {
        disabled: true
      }
    });

    expect(wrapper.hasClass('icui-button')).to.be.true;
    expect(wrapper.hasClass('icui-button--disabled')).to.be.true;

    const eventStub = sinon.stub(wrapper.vm, '$emit');
    wrapper.trigger('click');

    expect(eventStub.called).to.be.false;
  });
});
